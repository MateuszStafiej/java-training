package com.company;

public class Stack {


    int[] tab;
    int summit;

    public Stack(int MaxSize) {

        tab = new int[MaxSize];
        summit = 0;
    }

    boolean isEmpty() {
        if (summit == 0) {
            return true;
        } else
            return false;
    }

    void push(int E) throws ArrayIndexOutOfBoundsException {
        if (summit < tab.length) {
            tab[summit] = E;
            summit++;
        }
    }

    public int pop() {
        return tab[--summit];
    }

    public static void main(String[] args) {

        Stack thestack = new Stack(10);
        thestack.push(10);
        thestack.push(20);
        thestack.push(30);
        thestack.push(40);
        while (!thestack.isEmpty()) {
            long value = thestack.pop();
            System.out.print(value);
            System.out.print("  ");
        }
        System.out.println(" ");
    }


}

