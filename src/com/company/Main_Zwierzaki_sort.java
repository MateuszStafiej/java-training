package com.company;

import java.util.ArrayList;


public class Main_Zwierzaki_sort {
    public static void main(String[] args) {


        ArrayList<Zwierzak> x = new ArrayList<>();
        x.add(new Kot("Miał", 4));
        x.add(new Papuga("ćwir ćwir", 2));
        x.add(new Wąż("SSSSSsssssSSSS", 0));
        x.add(new Pająk("nic nie mówię, sio", 8));
        x.add(new Slowik("Ala kurwa!", 2));


        System.out.println("ilosc elementów " + x.size());
        for (Zwierzak it : x) {
            System.out.println(it.ilosc_nog + " " + it.daj_glos());
            try {
                it.uperdol_noge();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("***********************************************");
            System.out.println(it.ilosc_nog + " " + it.daj_glos());

        }

        System.out.println("--------------------------------------------------");
        SortZW(x);
        for (Zwierzak it : x) {
            System.out.println(it.ilosc_nog + " " + it.daj_glos());
        }


    }



    public static void SortZW(ArrayList<Zwierzak> tab) {


        boolean czyposortowane = true;
        for (int i = 0; i < tab.size() - 1; i++) {
            if (tab.get(i).ilosc_nog > tab.get(i + 1).ilosc_nog) {
                czyposortowane = false;
                break;
            }
        }
        if (czyposortowane == false) {
            for (int i = 0; i < tab.size(); i++) {
                for (int j = 1; j < tab.size() - i; j++) {
                    if (tab.get(j - 1).ilosc_nog > tab.get(j).ilosc_nog) {
                        swap(tab, j - 1, j);
                    }
                }
            }
        }


    }
//    public static int[] SortKotow(Zwierzak[]  tab ) {
//
//        int[] copy = new int[tab.length];
//        System.arraycopy(tab, 0, copy, 0, copy.length);
//
//        for (int i = 0; i < copy.length; i++) {
//            boolean sorted = true;
//            for (int j = 1; j < (copy.length - i); j++) {
//                if (copy[j - 1] > copy[j]) {
//                    swap(copy, j - 1, j);
//                    sorted = false;
//                }
//            }
//            if (sorted) {
//
//                return copy;
//
//
//            }
//        }
//
//        return copy;
//
//    }


//    public static int[] zwierzak_insertionSort(int[] ilosc_nog) {
//
//        int i;
//        for (int j = 1; j < ilosc_nog.length; j++) {
//            int key = ilosc_nog[j];
//            for (i = j - 1; (i >= 0) && (ilosc_nog[i] > key); i--) {
//                ilosc_nog[i + 1] = ilosc_nog[i];
//            }
//            ilosc_nog[i + 1] = key;
//        }
//
//        return ilosc_nog;
//    }


    public static void swap(ArrayList<Zwierzak> in, int first, int second) {
        Zwierzak temp = in.get(first);
        in.set(first, in.get(second));
        in.set(second, temp);


    }


}
