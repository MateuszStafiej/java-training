package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

    }

    public static int[] boubbleSort(int[] in) {
        int[] copy = new int[in.length];
        System.arraycopy(in, 0, copy, 0, copy.length);

        for (int i = 0; i < copy.length; i++) {
            boolean sorted = true;
            for (int j = 1; j < (copy.length - i); j++) {
                if (copy[j - 1] > copy[j]) {
                    swap(copy, j - 1, j);
                    sorted = false;
                }
            }
            if(sorted){
                return copy;
            }
        }

        return copy;
    }

    public static int[] insertionSort(int[] in) {

        int i; //ilość liczb ułożonych
        for (int j = 1; j < in.length; j++) {   // duży "for"
            int key = in[j];  //liczba do ułożenia,
            for (i = j - 1; (i >= 0) && (in[i] > key); i--) {  // mały "for" kod który idzie cały czas w lewo do momentu indeksu -1
                in[i + 1] = in[i];
            }
            in[i + 1] = key;
        }

        return in;
    }

    public static int[] swap(int[] in, int first, int second) { //OPERACJA PRZYGOTOWANA WCZEŚNIEJ
        int temp = in[first];
        in[first] = in[second];
        in[second] = temp;

        return in;
    }

}


