package com.company;

import static org.junit.Assert.*;

/**
 * Created by Urufu on 2016-04-21.
 */
public class MainTest {
    @org.junit.Test
    public void boubbleSort_sorted() {
        int[] in = {1, 2, 3};
        int[] expected = {1, 2, 3};

        int[] out = Main.boubbleSort(in);
        assertArrayEquals(expected, out);
    }

    @org.junit.Test
    public void boubbleSort_empty() {
        int[] in = {};
        int[] expected = {};

        int[] out = Main.boubbleSort(in);
        assertArrayEquals(expected, out);
    }

    @org.junit.Test
    public void boubbleSort_desending() {
        int[] in = {3, 2, 1};
        int[] expected = {1, 2, 3};

        int[] out = Main.boubbleSort(in);
        assertArrayEquals(expected, out);
    }

    @org.junit.Test
    public void boubbleSort_repetitive() {
        int[] in = {3, 3, 3};
        int[] expected = {3, 3, 3};

        int[] out = Main.boubbleSort(in);
        assertArrayEquals(expected, out);
    }

    @org.junit.Test
    public void insertionSort_sorted() {
        int[] in = {1, 2, 3};
        int[] expected = {1, 2, 3};

        int[] out = Main.insertionSort(in);
        assertArrayEquals(expected, in);
    }

    @org.junit.Test
    public void insertionSort_empty() {
        int[] in = {};
        int[] expected = {};

        int[] out = Main.insertionSort(in);
        assertArrayEquals(expected, in);
    }

    @org.junit.Test
    public void insertionSort_desending() {
        int[] in = {4, 3, 2, 1};
        int[] expected = {1, 2, 3, 4};

        int[] out = Main.insertionSort(in);
        assertArrayEquals(expected, in);
    }

    @org.junit.Test
    public void insertionSort_repetitive() {
        int[] in = {3, 3, 3};
        int[] expected = {3, 3, 3};

        int[] out = Main.insertionSort(in);
        assertArrayEquals(expected, in);
    }

    @org.junit.Test
    public void insertionSort_basic() {
        int[] in = {7, 6, 8, 2, 3};
        int[] expected = {2, 3, 6, 7, 8};

        int[] out = Main.insertionSort(in);
        assertArrayEquals(expected, in);
    }

    @org.junit.Test
    public void swap_basic() {
        int[] in = {1, 2, 3};
        Main.swap(in, 0, 1);
        int[] expected = {2, 1, 3};

        assertArrayEquals(expected, in);
    }


}
