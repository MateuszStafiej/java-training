package com.company;

public class Test_Zwierzaka {
    public static void main(String[] args) {


        Kot kot = new Kot("miał miał skurwysynu :3", 4);
        Papuga papuga = new Papuga("ćwir ćwir", 2);
        Waz waz = new Waz("SssssSSSssssSSS", 0);
        Pajak pajak = new Pajak("przecież ja nic nie mówie", 6);

        System.out.println(kot.daj_glos());
        System.out.println(papuga.daj_glos());
        System.out.println(waz.daj_glos());
        System.out.println(pajak.daj_glos());

        System.out.println(kot.ilosc_nog);
        System.out.println(papuga.ilosc_nog);
        System.out.println(waz.ilosc_nog);
        System.out.println(pajak.ilosc_nog);

        try {
            kot.uperdol_noge();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(kot.ilosc_nog);


    }


}
